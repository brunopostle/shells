shells.pdf : shells.tex shells.bib mdpi.bst mdpi.cls Makefile
	pdflatex shells
	bibtex shells
	pdflatex shells
	pdflatex shells

shells.zip : clean
	zip shells.zip shells.tex shells.bib mdpi.bst mdpi.cls Makefile *.pdf *.jpg

clean : 
	-rm -f shells.aux shells.log shells.out shells.bbl shells.blg shells.pdf shells.zip

html : shells.tex
	latex2html -split 0 shells.tex

spell : shells.tex
	aspell --mode=tex --check $<

%.png : %.svg
	rsvg $< $@

.PHONY : clean html spell
