High Average Low No Answer

* Originality / Novelty
( )       ( )       (x)       ( )

* Significance of Content
( )       (x)       ( )       ( )

* Quality of Presentation
( )       (x)       ( )       ( )

* Scientific Soundness
( )       (x)       ( )       ( )

* Interest to the readers
( )       (x)       ( )       ( )

* Overall Merit
( )       (x)       ( )       ( )


* Overall Recommendation
( )  Accept in present form
(x)  Accept after minor revision, I do not need to see the revised version
( )  Reconsider after major revision, I want to see the revised version
( )  Reject


* English Language and Style
(x)  English language and style are fine
( )  Minor spell check required
( )  Extensive editing of English language and style required
( )  I don't feel qualified to judge about the English Language and Style


Comments and Suggestions for Authors

* Comments and Suggestions for Authors
Although the paper doesn't really present totally new ways of designing developable surfaces, it contains some useful (to geometers known) observations which could help in practice. I like the artistic touch more than the methods themselves.

Here are some suggestions for a minor revision:

There are no mathematical justifications for the methods, basically because they are pretty obvious. The description isn't really precise. I would suggest to
avoid perspective projection in explanatory figures and replace it by orthographic or
a more general parallel projection, to make parallel lines in space more apparent in the figures.

Method 2 is explicitly discussed as an example in the book "Architectural Geometry" by Pottmann et al; one may mention that it is important to use the same weights for the 2 splines if NURBS are employed (which is also important in the other methods where several splines are involved). The book would also be a good reference for the general discussion in section 3.

The application of a projective transformation as in section 9 can be much more general. Why using such a special transformation matrix? Any regular 4x4 matrix would generate a projective map which will map a developable surface into another developable surface.


Review Comment Sharing
( )  My comments for authors can be viewed by other reviewers if they request to. (In all cases my identity and my comments for editors remain strictly confidential.)
( )  Do not share with other reviewers my comments for authors.

Date & Signature

Date of manuscript submission
16 July 2012 21:04:32

Date of this review
21 August 2012 15:43:24
